data:extend(
        {
            {
                type = "recipe",
                name = "woodpellets2-fertiliser",
                category = "chemistry",
                energy_required = 45,
                enabled = true,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                ingredients = {
                    { type = "item", name = "wood-pellets", amount = 5 },
                },
                results = {
                    { type = "item", name = "fertiliser", amount = 6 }
                }
            },
            {
                type = "recipe",
                name = "wood2-coal",
                energy_required = 15,
                enabled = true,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                category = "chemistry",
                ingredients = {
                    { type = "item", name = "wood", amount = 4 },
                    { type = "item", name = "coal", amount = 1 }
                },
                results = {
                    { type = "item", name = "coal", amount = 4 }
                }
            },
            {
                type = "recipe",
                name = "wood2-oil",
                energy_required = 10,
                enabled = true,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                category = "chemistry",
                ingredients = {
                    { type = "item", name = "wood", amount = 22 },
                    { type = "item", name = "coal", amount = 2 },
                    { type = "fluid", name = "water", amount = 30 }
                },
                results = {
                    { type = "fluid", name = "light-oil", amount = 65 }
                }
            },
            {
                type = "recipe",
                name = "air2-water",
                energy_required = 100,
                enabled = true,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                category = "chemistry",
                ingredients = {
                },
                results = {
                    { type = "fluid", name = "water", amount = 200 }
                }
            },
            {
                type = "recipe",
                name = "water2-plantwater",
                energy_required = 40,
                enabled = true,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                category = "chemistry",
                ingredients = {
                    { type = "fluid", name = "water", amount = 100 },
                    { type = "item", name = "fertiliser", amount = 2 }
                },
                results = {
                    { type = "fluid", name = "plantwater", amount = 99 }
                }
            },
            {
                type = "recipe",
                name = "plantwater2-wood",
                icon = "__bobgreenhouse__/graphics/icons/seedling.png",
                icon_size = 32,
                subgroup = "bob-greenhouse-items",
                order = "g[greenhouse-cycle-1]",
                category = "bob-greenhouse",
                energy_required = 50,
                hide_from_player_crafting = true,
                always_show_made_in = true,
                ingredients =
                {
                    {type = "item", name = "seedling", amount = 10},
                    {type = "fluid", name = "plantwater", amount = 20}
                },
                results =
                {
                    {type = "item", name = "wood", amount_min = 25, amount_max = 35},
                },
                allow_decomposition = false
            }
        }
)