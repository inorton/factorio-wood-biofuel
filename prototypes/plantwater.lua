data:extend(
    {
        {
            type = "fluid",
            name = "plantwater",
            localised_name = "Plant Water",
            default_temperature = 22,
            max_temperature = 90,
            heat_capacity = "0.3KJ",
            base_color = {r=0, g=0.94, b=0.6},
            flow_color = {r=0.7, g=1.0, b=0.7},
            icon = "__base__/graphics/icons/fluid/water.png",
            icon_size = 64,
            icon_mipmaps = 4,
            order = "a[fluid]-a[water]"
        }
    })